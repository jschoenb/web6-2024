<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
</head>
    <body>
        <ul>
            @foreach($books as $book)
                <li><a href="books/{{$book->id}}">{{$book->isbn}} {{$book->title}} {{$book->subtitle}}</a></li>
            @endforeach
        </ul>
    </body>
</html>
