<?php

use App\Models\Book;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\BookController::class,"index"]);
Route::get('/books', [\App\Http\Controllers\BookController::class,"index"]);

Route::get('/books/{id}', function ($id) {
    $book = Book::find($id);
    return view('books.show',compact('book'));
});
