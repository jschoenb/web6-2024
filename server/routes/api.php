<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('books',[BookController::class,'index']);
Route::get('books/{isbn}',[BookController::class,'findByISBN']);
Route::get('books/checkisbn/{isbn}',[BookController::class,'checkISBN']);
Route::get('books/search/{searchTerm}',[BookController::class,'findBySearchTerm']);


Route::post('auth/login',[AuthController::class,'login']);

Route::group(['middleware' =>['api','auth.jwt','auth.admin']],function(){
    Route::post('books',[BookController::class,'save']);
    Route::put('books/{isbn}',[BookController::class,'update']);
    Route::delete('books/{isbn}',[BookController::class,'delete']);
    Route::post('auth/logout',[AuthController::class,'logout']);
});

Route::group(['middleware' =>['api','auth.jwt']],function(){
    Route::post('auth/logout',[AuthController::class,'logout']);
});

