<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\Image;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DateTime;
use Illuminate\Support\Facades\DB;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $book = new Book();
        $book->title = 'Herr der Ringe';
        $book->isbn = '1234567891';
        $book->subtitle = 'Die Gefährten';
        $book->rating = 10;
        $book->published = new DateTime();

        $user = User::first();
        $book->user()->associate($user);
        //rauslöschen dissociate

        //in die DB speichern
        $book->save();

        //add images to book
        $image1 = new Image();
        $image1->title = "Cover 1";
        $image1->url = "https://m.media-amazon.com/images/I/71pio-YV3XL._SY466_.jpg";

        $image2 = new Image();
        $image2->title = "Cover 2";
        $image2->url = "https://m.media-amazon.com/images/I/51193ZLozAL._SY445_SX342_.jpg";

        $book->images()->saveMany([$image1,$image2]);

        //add authors
        $authors = Author::all()->pluck("id");
        $book->authors()->sync($authors);

        $book->save();
    }
}
