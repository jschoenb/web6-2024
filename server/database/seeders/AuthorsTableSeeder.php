<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $author1 = new \App\Models\Author;
        $author1->firstName = "Max";
        $author1->lastName = "Maier";
        $author1->save();

        $author2 = new \App\Models\Author;
        $author2->firstName = "Susi";
        $author2->lastName = "Musterfrau";
        $author2->save();

        $author3 = new \App\Models\Author;
        $author3->firstName = "Karl";
        $author3->lastName = "Huber";
        $author3->save();
    }
}
