import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {BookFactory} from "../shared/book-factory";
import {BookStoreService} from "../shared/book-store.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BookFormErrorMessages} from "./book-form-error-messages";
import {Book} from "../shared/book";
import {BookValidators} from "../shared/book-validators";

@Component({
  selector: 'bs-book-form',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  templateUrl: './book-form.component.html',
  styles: ``
})
export class BookFormComponent implements OnInit{
  bookForm : FormGroup;
  book = BookFactory.empty();
  isUpdatingBook = false;
  images: FormArray;
  errors:{[key:string]:string} = {};

  constructor(
    private fb: FormBuilder,
    private bs : BookStoreService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.bookForm = this.fb.group({});
    this.images = this.fb.array([]);
  }

  ngOnInit() {
    const isbn = this.route.snapshot.params['isbn'];
    if(isbn){ // we're updating an existing book
      this.isUpdatingBook = true;
      this.bs.getSingle(isbn).subscribe(book =>{
        this.book = book;
        this.initBook();
      });
    }
    this.initBook();
  }

  initBook() {
    this.buildThumbnailsArray();
    this.bookForm = this.fb.group({
      id: this.book.id,
      title: [this.book.title,Validators.required],
      subtitle: this.book.subtitle,
      isbn:[this.book.isbn,[
        Validators.required,
        BookValidators.isbnFormat
      ],this.isUpdatingBook?null:BookValidators.isbnExists(this.bs)],
      description: this.book.description,
      rating: [this.book.rating,[
        Validators.required,
        Validators.min(0),
        Validators.max(10)
      ]],
      images: this.images,
      published: [this.book.published,Validators.required]
    });

    this.bookForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  private buildThumbnailsArray(){
    if(this.book.images){
      this.images = this.fb.array([]);
      for(let img of this.book.images){
        let fg = this.fb.group({
          id: this.fb.control(img.id),
          url: this.fb.control(img.url,[Validators.required]),
          title: this.fb.control(img.title,Validators.required)
        });
        this.images.push(fg);
      }
    }
  }

  addThumbnailControl() {
    this.images.push(this.fb.group({id:0,url:null,title:null}));
  }

  submitForm() {
      this.bookForm.value.images = this.bookForm.value.images.filter((thumbnail:{url:string}) => thumbnail.url);

      const book : Book = BookFactory.fromObject(this.bookForm.value);
      //Hack for Authors
      book.authors = this.book.authors;
      console.log(book);
      if(this.isUpdatingBook){
        this.bs.update(book).subscribe(() => {
          this.router.navigate(['../../books',book.isbn],{relativeTo:this.route});
        });
      } else {
        //Hack
        book.user_id = 1;
        this.bs.create(book).subscribe(() => {
          this.book = BookFactory.empty();
          this.bookForm.reset(BookFactory.empty());
          this.router.navigate(['../books'],{relativeTo:this.route});
        });
      }
  }


  private updateErrorMessages() {
    this.errors = {};
    for(const message of BookFormErrorMessages){
        const control = this.bookForm.get(message.forControl)
        if(control && control.dirty && control.invalid &&
         control.errors && control.errors[message.forValidator] &&
          !this.errors[message.forControl]) {
          this.errors[message.forControl] = message.text;
        }
    }
  }
}
