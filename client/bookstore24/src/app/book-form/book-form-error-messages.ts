
export class ErrorMessage{
  constructor(
    public forControl: string,
    public forValidator: string,
    public text: string){}
}

export const BookFormErrorMessages =[
    new ErrorMessage('title','required','Ein Buchtitel muss angegeben werden'),
    new ErrorMessage('isbn','required','Es muss eine ISBN angegeben werden'),
    new ErrorMessage('isbn','isbnFormat','Die ISBN muss aus 10 oder 13 Zeichen bestehen'),
    new ErrorMessage('isbn','isbnExists','Die ISBN existiert bereits'),
    new ErrorMessage('rating','required','Bitte vergeben Sie eine Bewertung'),
    new ErrorMessage('rating','min','Die Bewertung muss mindestens 0 sein'),
    new ErrorMessage('rating','max','Die Bewertung darf höchstens 10 sein'),
    new ErrorMessage('published','required','Bitte geben Sie das Erscheinungsdatum an')
]
