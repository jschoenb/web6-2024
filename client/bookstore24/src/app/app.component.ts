import { Component } from '@angular/core';
import {RouterLink, RouterLinkActive, RouterOutlet} from '@angular/router';
import {BookListComponent} from "./book-list/book-list.component";
import {BookDetailsComponent} from "./book-details/book-details.component";
import {AuthenticationService} from "./shared/authentication.service";

@Component({
  selector: 'bs-root',
  standalone: true,
  imports: [RouterOutlet, BookListComponent, BookDetailsComponent, RouterLink, RouterLinkActive],
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(private authService: AuthenticationService) {
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  getLoginLabel(){
    return this.isLoggedIn() ? "Logout" : "Login";
  }
}
